import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PipesComponent } from './components/pipes/pipes.component';
import { Pipe } from '../.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PipesComponent,
    Pipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
