import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-childs',
  templateUrl: './childs.component.html',
  styleUrls: ['./childs.component.css']
  
})
export class ChildsComponent implements OnInit {
  mensaje = 'Mensaje del hijo al padre';

  @Input() ChildMessage!: string;
  @Output() EventoMensaje = new EventEmitter<string>();

  constructor() { }


  ngOnInit(): void {
    console.log(this.ChildMessage);
    this.EventoMensaje.emit(this.mensaje);

  }
  
}






