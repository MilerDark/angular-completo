import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ParentsComponent } from './components/parents/parents.component';
import { ChildsComponent } from './components/childs/childs.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentsComponent,
    ChildsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
